export default {
  primary: '#FEAA00',
  secondary: '#039be5',
  black: '#222222',
  white: '#ffffff',
  ghostWhite: '#F9FAFB',
  lightGrey: '#f9f9f9',
  mediumGrey: '#6e6869',
  backgroundColour: '#eaedf0',
  red: '#fc5c65',
  facebook: '#3b5998',
  google: '#DB4437',
  skyblue: '#00BFDF'
};
