import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import WelcomeScreen from '../screens/WelcomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';

import Colors from '../utils/colors';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator initialRouteName="Welcome">
      <Stack.Screen name="Welcome" component={WelcomeScreen} options={{ headerShown: false }}/>
      <Stack.Screen name="Login" component={LoginScreen} options={{ headerTintColor: Colors['black'], title: 'Sign in', headerTitleStyle: { alignSelf: 'center' } }}/>
      <Stack.Screen name="Register" component={RegisterScreen} options={{ headerTintColor: Colors['black'], title: 'Sign up with Email', headerTitleStyle: { alignSelf: 'center' } }} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={{ headerShown: false }}/>
    </Stack.Navigator>
  );
}
