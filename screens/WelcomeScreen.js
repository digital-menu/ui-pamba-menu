import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';

import AppButton from '../components/AppButton';
import Colors from '../utils/colors';
import useStatusBar from '../hooks/useStatusBar';

export default function WelcomeScreen({ navigation }) {
  useStatusBar('light-content');

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require('../assets/flame.png')} style={styles.logo} />
      </View>
      <View style={styles.buttonContainer}>
        <AppButton title="Sign up with Email" onPress={() => navigation.navigate('Register')} />
        <AppButton iconType="AntDesign" iconName="apple-o" iconColor="primary" color="ghostWhite" textColor="black" title="Sign up with Apple" onPress={() => navigation.navigate('Register')} />
        <AppButton iconType="SimpleLineIcons" iconName="social-facebook" iconColor="primary" color="ghostWhite" textColor="black" title="Continue with Facebook" onPress={() => navigation.navigate('Register')} />
        <AppButton iconType="SimpleLineIcons" iconName="social-google" iconColor="primary" color="ghostWhite" textColor="black" title="Continue with  Google" onPress={() => navigation.navigate('Register')} />

        <View style={styles.footerButtonContainer}>
        <Text>
          <Text style={styles.loginButtonText}>Already a member ? </Text>
          <Text style={styles.loginButtonclickText} onPress={() => navigation.navigate('Login')}>Sign in</Text>
        </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: Colors.backgroundColour
  },
  logoContainer: {
    position: 'absolute',
    top: 60,
    alignItems: 'center'
  },
  logo: {
    width: 125,
    height: 125
  },
  buttonContainer: {
    padding: 20,
    paddingBottom: 60,
    width: '100%'
  },
  footerButtonContainer: {
    marginVertical: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginButtonText: {
    color: Colors.black,
    fontSize: 18,
    fontWeight: '600'
  },
  loginButtonclickText: {
    color: Colors.skyblue,
    fontSize: 18,
    fontWeight: '600'
  }


});
