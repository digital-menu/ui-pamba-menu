import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Switch } from 'react-native';
import * as Yup from 'yup';

import Colors from '../utils/colors';
import SafeView from '../components/SafeView';
import Form from '../components/Forms/Form';
import FormField from '../components/Forms/FormField';
import FormButton from '../components/Forms/FormButton';
import IconButton from '../components/IconButton';
import FormErrorMessage from '../components/Forms/FormErrorMessage';
import { registerWithEmail } from '../components/Firebase/firebase';
import useStatusBar from '../hooks/useStatusBar';

const validationSchema = Yup.object().shape({
  fname: Yup.string()
    .required()
    .label('First Name'),
  lname: Yup.string()
    .required()
    .label('Last Name'),
  email: Yup.string()
    .required('Please enter a valid email')
    .email()
    .label('Email'),
  password: Yup.string()
    .required()
    .min(6, 'Password must have at least 6 characters')
    .label('Password')
});

export default function RegisterScreen({ navigation }) {
  useStatusBar('light-content');

  const [passwordVisibility, setPasswordVisibility] = useState(true);
  const [rightIcon, setRightIcon] = useState('eye');

  const [registerError, setRegisterError] = useState('');

  const [switchValue, setSwitchValue] = useState(false);

  const toggleSwitch = (value) => {
    setSwitchValue(value);
  };

  function handlePasswordVisibility() {
    if (rightIcon === 'eye') {
      setRightIcon('eye-off');
      setPasswordVisibility(!passwordVisibility);
    } else if (rightIcon === 'eye-off') {
      setRightIcon('eye');
      setPasswordVisibility(!passwordVisibility);
    }
  }

  async function handleOnSignUp(values, actions) {
    const { fname, lname, email, password } = values;
    const name = `${fname} ${lname}`;
    if(switchValue) {
      try {
        await registerWithEmail(email, password);
      } catch (error) {
        setRegisterError(error.message);
      }
    } else {
      setRegisterError("You must have accept terms of Service");
    }
  }

  return (
    <SafeView style={styles.container}>
      
      <Form
        initialValues={{
          fname: '',
          lname: '',
          email: '',
          password: '',
          confirmPassword: ''
        }}
        validationSchema={validationSchema}
        onSubmit={values => handleOnSignUp(values)}
      >
        <FormField
          name="fname"
          displayTitle="First name"
          leftIcon="account"
          placeholder="Enter First name"
          autoFocus={true}
        />
        <FormField
          name="lname"
          displayTitle="Last name"
          leftIcon="account"
          placeholder="Enter Last name"
        />
        <FormField
          name="email"
          displayTitle="Email"
          leftIcon="email"
          placeholder="Enter email"
          autoCapitalize="none"
          keyboardType="email-address"
          textContentType="emailAddress"
        />
        <FormField
          name="password"
          displayTitle="Password"
          leftIcon="lock"
          placeholder="Enter password"
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry={passwordVisibility}
          textContentType="password"
          rightIcon={rightIcon}
          handlePasswordVisibility={handlePasswordVisibility}
        />
        <View style={{flex: 1, flexDirection: 'row', alignContent: 'space-between'}}>
          <View style={{flex: 2, justifyContent: 'space-between',}}>
            <Text>
              Creating an account means you are okay with our Terms of Service, Privacy Policy and our default Notification Settings
            </Text>
          </View>
          <View style={{flex: 1, justifyContent: 'space-between'}}>
            <Switch
              style={{marginTop: 30}}
              onValueChange={toggleSwitch}
              value={switchValue}
            />
          </View>
        </View>
        <FormButton title={'Create Account'} />
        {<FormErrorMessage error={registerError} visible={true} />}
      </Form>
      <Text>
          <Text style={styles.loginButtonText}>Already a member ? </Text>
          <Text style={styles.loginButtonclickText} onPress={() => navigation.navigate('Login')}>Sign in</Text>
      </Text>
    </SafeView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: Colors.backgroundColour
  },
  backButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10
  },
  footerButtonContainer: {
    marginVertical: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginButtonText: {
    color: Colors.black,
    fontSize: 18,
    fontWeight: '600'
  },
  loginButtonclickText: {
    color: Colors.skyblue,
    fontSize: 18,
    fontWeight: '600',
  }
});
