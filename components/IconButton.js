import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons, AntDesign, Ionicons, SimpleLineIcons } from '@expo/vector-icons';

export default function IconButton({ iconName, color, size, onPress, style, iconType="MaterialCommunityIcons" }) {
  return (
    <TouchableOpacity style={[style]} onPress={onPress}>
      {(iconType === 'MaterialCommunityIcons') && <MaterialCommunityIcons name={iconName} size={size} color={color} />}
      {(iconType === 'AntDesign') && <AntDesign name={iconName} size={size} color={color} />}
      {(iconType === 'Ionicons') && <Ionicons name={iconName} size={size} color={color} />}
      {(iconType === 'SimpleLineIcons') && <SimpleLineIcons name={iconName} size={size} color={color} />}
    </TouchableOpacity>
  );
}
