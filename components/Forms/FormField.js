import React from 'react';
import { useFormikContext } from 'formik';
import { View, Text, StyleSheet } from 'react-native';

import AppTextInput from '../AppTextInput';
import FormErrorMessage from './FormErrorMessage';
import Colors from '../../utils/colors';

export default function FormField({ name, displayTitle,  width, ...otherProps }) {
  const {
    setFieldTouched,
    setFieldValue,
    values,
    errors,
    touched
  } = useFormikContext();

  return (
    <React.Fragment>
      {(displayTitle) && <View><Text style={styles.titleText}>{displayTitle}</Text></View>}
      <AppTextInput
        value={values[name]}
        onChangeText={text => setFieldValue(name, text)}
        onBlur={() => setFieldTouched(name)}
        width={width}
        {...otherProps}
      />
      <FormErrorMessage error={errors[name]} visible={touched[name]} />
    </React.Fragment>
  );
}


const styles = StyleSheet.create({
  
  titleText: {
    color: Colors.black,
    fontSize: 18,
    fontWeight: '600'
  }
});
