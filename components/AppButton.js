import React from 'react';
import IconButton from '../components/IconButton';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

import Colors from '../utils/colors';

export default function AppButton({ title, onPress, color = 'primary', textColor = 'white', iconType='MaterialCommunityIcons', iconName='', iconColor='white' }) {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: Colors[color] }]}
      onPress={onPress}
    >
      { (iconName !== '') && 
        <IconButton
            style={styles.smalliconButton}
            iconName={iconName}
            color={Colors[iconColor]}
            size={30}
            iconType={iconType}
        />
      }
      <Text style={[styles.buttonText, { color: Colors[textColor] }]}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  smalliconButton: {
    position: 'absolute',
    justifyContent: 'center',
    left: 5
  },
  button: {
    marginVertical: 10,
    borderRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    width: '100%'
  },
  buttonText: {
    fontSize: 18,
    fontWeight: '600',
    textTransform: 'uppercase'
  }
});
